# KiSeratus

KiSeratus, adalah aplikasi unik yang menyediakan beberapa fitur-fitur yang dapat membantu
Tukang untuk mengerjakan tugasnya , yang mana bisa di akses oleh orang-orang yang sudah
terdaftar sebelumnya.

## How to run this app
1. Clone this repo
```
git clone git@gitlab.com:annaswanda/fe-kiseratus.git
``` 
2. Enter to root directory of this project "fe-kiseratus"
```
cd fe-kiseratus 
``` 
3. Install Depedencies
```
yarn
```
4. Start Development
```
yarn start
```