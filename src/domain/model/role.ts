import { uid } from "uid";
import { IMenu, Menu } from "./menu";

export interface IRole {
    id?: string;
    name: string;
    menus?: IMenu[];
}

export class Role {
    private _id: string;
    private _props: IRole;
    constructor(props: IRole) {
        const { id, ...data } = props;
        this._id = id || uid();
        this._props = data;
    }
    static create(props: IRole): Role {
        return new Role(props);
    }
    unmarshall(): IRole {
        return {
            id: this.id,
            name: this.name,
            menus: this.menus?.map((item) => item.unmarshall()) || [],
        };
    }
    get id(): string {
        return this._id;
    }
    get name(): string {
        return this._props.name;
    }
    get menus(): Menu[] {
        return this._props.menus && this._props.menus.length > 0
            ? this._props.menus.map((item) => Menu.create(item))
            : [];
    }
}
