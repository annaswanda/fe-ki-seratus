import { uid } from "uid";

export interface IPermission {
    id?: string;
    name: string;
}

export class Permission {
    private _id: string;
    private _props: IPermission;
    constructor(props: IPermission) {
        const { id, ...data } = props;
        this._id = id || uid();
        this._props = data;
    }
    static create(props: IPermission): Permission {
        return new Permission(props);
    }
    get id(): string {
        return this._id;
    }
    get name(): string {
        return this._props.name;
    }
}
