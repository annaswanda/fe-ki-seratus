export interface IEvent {
    date: Date;
    event: string;
}
