import { uid } from "uid";

export interface IMenu {
    id?: string;
    name: string;
    url: string;
    icon: string;
    active: boolean;
}
 
export class Menu {
    private _id: string;
    private _props: IMenu;
    constructor(props: IMenu) {
        const { id, ...data } = props;
        this._id = id || uid();
        this._props = data;
    }
    static create(props: IMenu): Menu {
        return new Menu(props);
    }
    unmarshall(): IMenu {
        return {
            id: this.id,
            name: this.name,
            url: this.url,
            icon: this.icon,
            active: this.active,
        };
    }
    get id(): string {
        return this._id;
    }
    get name(): string {
        return this._props.name;
    }
    get url(): string {
        return this._props.url;
    }
    get icon(): string {
        return this._props.icon;
    }
    get active(): boolean {
        return this._props.active;
    }
}
