import { uid } from "uid";
import { IRole, Role } from "./role";

export interface IUser {
    id?: string;
    name: string;
    id_login: string;
    password: string;
    role: IRole | null;
}

export class User {
    private _props: IUser;
    private _id: string;
    constructor(props: IUser) {
        this._id = props.id || uid();
        this._props = props;
    }
    static create(props: IUser): User {
        return new User(props);
    }
    unmarshall(): IUser {
        return {
            id: this.id,
            name: this.name,
            id_login: this.id_login,
            password: this.password,
            role: this.role?.unmarshall() || null,
        };
    }
    get id(): string {
        return this._id;
    }
    get name(): string {
        return this._props.name;
    }
    get id_login(): string {
        return this._props.id_login;
    }
    get password(): string {
        return this._props.password;
    }
    get role(): Role | null {
        return this._props.role ? Role.create(this._props.role) : null;
    }
}
