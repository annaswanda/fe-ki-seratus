import { uid } from "uid";

export interface IFormula {
    id?: string;
    type: "number" | "operator";
    value: string;
}

export class Formula {
    private _id: string;
    private _props: IFormula;
    constructor(props: IFormula) {
        const { id, ...data } = props;
        this._id = id || uid();
        this._props = data;
    }
    static create(props: IFormula): Formula {
        return new Formula(props);
    }
    addNumber(number: number):Formula {
        this._props.value = `${this._props.value}${number}`;
        return this
    }
    unmarshall(): IFormula {
        return {
            id: this.id,
            type: this.type,
            value: this.value,
        };
    }
    get id(): string {
        return this._id;
    }
    get type(): "number" | "operator" {
        return this._props.type;
    }
    get value(): string {
        return this._props.value;
    }
}
