import { User } from "../model/user";

export interface IUserRepository {
    login(id: string, password: string): Promise<User>;
    logout(): Promise<boolean>;
    me(): Promise<User>;
}
