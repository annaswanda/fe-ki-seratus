import { Box, Text, Button, useDisclosure, Input } from "@chakra-ui/react";
import React, { useCallback, useState, useEffect, useRef } from "react";
import ReactFlow, { useNodesState, useEdgesState, addEdge } from "reactflow";
import "reactflow/dist/style.css";
import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  IconButton,
} from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "@chakra-ui/react";
import { AddIcon, ChevronDownIcon, Icon } from "@chakra-ui/icons";
import { HiDotsVertical } from "react-icons/hi";

const initialNodes: Array<Object | any | unknown> = [
  {
    id: "horizontal-1",
    sourcePosition: "right",
    targetPosition: "left",
    data: { label: "Input" },
    position: { x: 0, y: 0 },
  },
  {
    id: "horizontal-2",
    sourcePosition: "right",
    targetPosition: "left",
    data: { label: "A Node" },
    position: { x: 200, y: 0 },
  },
  {
    id: "horizontal-3",
    sourcePosition: "right",
    targetPosition: "left",
    data: { label: "Node 3" },
    position: { x: 200, y: 80 },
  },
  {
    id: "horizontal-4",
    sourcePosition: "right",
    targetPosition: "left",
    data: { label: "Node 4" },
    position: { x: 400, y: 0 },
  },
  {
    id: "horizontal-5",
    sourcePosition: "right",
    targetPosition: "left",
    data: { label: "Node 4" },
    position: { x: 400, y: 80 },
  },
];

const initialEdges = [
  {
    id: "horizontal-e1-2",
    source: "horizontal-1",
    type: "smoothstep",
    target: "horizontal-2",
  },
  {
    id: "horizontal-e1-3",
    source: "horizontal-1",
    type: "smoothstep",
    target: "horizontal-3",
  },
  {
    id: "horizontal-e2-4",
    source: "horizontal-2",
    type: "smoothstep",
    target: "horizontal-4",
  },
  {
    id: "horizontal-e2-5",
    source: "horizontal-2",
    type: "smoothstep",
    target: "horizontal-5",
  },
];

export const SilsilahKeluargaView = () => {
  const [nodes, _, onNodesChange] = useNodesState(initialNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);
  const [indexNode, setIndexNode] = useState<any>(null);
  const [nodeDataByIndex, setNodeDataByIndex] = useState<Object | any>({});
  const { isOpen, onOpen, onClose } = useDisclosure();
  const reactFlowWrapper = useRef<any>(null);
  const onConnect = useCallback(
    (params: any) => setEdges((els) => addEdge(params, els)),
    []
  );

  useEffect(() => {
    setNodeDataByIndex({
      name: initialNodes[indexNode]?.data?.label,
      action: null,
    });
  }, [indexNode]);

  return (
    <Box w="100%" h="840px" border="1px" ref={reactFlowWrapper}>
      <ReactFlow
        nodes={nodes}
        edges={edges}
        nodesDraggable={true}
        nodesConnectable={true}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        onConnectStart={(event)=>{
            const { top, left } = reactFlowWrapper.current.getBoundingClientRect();
            // const startData = start.getBoundingClientRect();
            // event.clientX - left - 75, y: event.clientY - top
        }}
        onConnect={onConnect}
        onNodeClick={(e, node) => {
          nodes
            ?.map((e) => e?.id)
            .map((e, i) => {
              if (e == node?.id) {
                setIndexNode(i);
              }
            });
        }}
        fitView
        attributionPosition="bottom-left"
      >
        <Button
          colorScheme="teal"
          variant="solid"
          position="absolute"
          bottom="10px"
          right="10px"
          boxShadow="xl"
          bg="cyan.600"
          w="60px"
          h="60px"
          zIndex="4"
          display="flex"
          alignItems="center"
          justifyContent="center"
          onClick={onOpen}
        >
          <AddIcon color="white" />
        </Button>

        <Box
          position="absolute"
          top="10px"
          right="10px"
          boxShadow="xl"
          bg="cyan.600"
          px="15px"
          h="60px"
          display="flex"
          alignItems="center"
          zIndex="4"
          borderRadius="md"
        >
          <Box display="flex" gap="20px" alignItems="center">
            <Box display="flex" gap="10px" as="b" color="white">
              <Text>Nama: </Text>
              <Text>{indexNode >= 0 ? nodeDataByIndex?.name : "-"}</Text>
            </Box>
            <Menu>
              <MenuButton
                as={IconButton}
                aria-label="Options"
                icon={<Icon as={HiDotsVertical} w="5" h="5" color="black" />}
                variant="outline"
                bg="white"
              />
              <MenuList>
                <MenuItem onClick={() => console.log(1)}>Edit</MenuItem>
                <MenuItem onClick={() => console.log(1)}>Delete</MenuItem>
              </MenuList>
            </Menu>
          </Box>
        </Box>
      </ReactFlow>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Tambah Data Keluarga</ModalHeader>
          <ModalBody display="flex" flexDirection="column" gap="20px">
            <Box>
              <Text>Nama</Text>
              <Input placeholder="Input name" />
            </Box>
            <Box display="flex" flexDirection="column" gap="10px">
              <Text>Pilih Nama Keluarga</Text>
              <Menu>
                <MenuButton as={Button} rightIcon={<ChevronDownIcon />}>
                  Pilih Nama Keluarga
                </MenuButton>
                <MenuList>
                    {
                        nodes?.map((node, i)=>{
                            return(
                                <MenuItem>{node?.data?.label}</MenuItem>
                            )
                        })
                    }
                </MenuList>
              </Menu>
            </Box>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3}>
              Save
            </Button>
            <Button variant="ghost" onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
};
