import { Container } from "@chakra-ui/react";
import React from "react";
import FormulaTable from "./components/table/FormulaTable";

type Props = {};

const FormulaPage = (props: Props) => {
  return (
    <div style={{ padding: 10 }}>
      <h1>Halaman Formula</h1>
      <FormulaTable />
    </div>
  );
};

export default FormulaPage;
