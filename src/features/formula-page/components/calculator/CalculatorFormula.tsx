import {
  Box,
  Button,
  Container,
  Editable,
  EditableInput,
  EditablePreview,
} from "@chakra-ui/react";
import { useState } from "react";
import { btnValues } from "../../utils";
type CalculatorFormulaProps = {
  close: () => {};
};

const CalculatorFormula = ({ close }: CalculatorFormulaProps) => {
  const [var1, setVar1] = useState([]);
  const [var2, setVar2] = useState([]);
  const [var3, setVar3] = useState([]);
  const [operator1, setOperator1] = useState("");
  const [operator2, setOperator2] = useState("");

  const checkIsNumber = (str: string) => {
    var pattern = /^[+-]?\d+(\.\d+)?$/;
    // console.log(pattern.test(str), "asb");
    return pattern.test(str); // returns a boolean
  };

  const numClickHandler = (val: string) => {
    if (val !== ".") {
      if (operator1 === "") {
        let dataDetail = [...var1, val];
        // setVar1(dataDetail);
      }
    }
  };
  console.log(var1, "@calculatorValue");
  const handleClick = (data: string) => {
    const validNum = checkIsNumber(data);
    if (data === "." || validNum === true) {
      console.log("is number or .");
      // setCalculatorValue(value);
      numClickHandler(data);
    }
  };
  return (
    <Container
      sx={{
        backgroundColor: "#433e3e",
        padding: 2,
        borderRadius: 10,
        color: "#ffff",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-around",
          backgroundColor: "#0000",
          alignItems: "center",
          borderTopLeftRadius: 5,
          borderTopRightRadius: 5,
        }}
      >
        <p>Nama : </p>
        <Editable
          sx={{
            border: "1px solid grey",
            padding: 1,
            borderRadius: 10,
            marginBottom: 1,
          }}
          defaultValue="Take some chakra"
        >
          <EditablePreview />
          <EditableInput />
        </Editable>
      </Box>
      <Box
        sx={{
          textAlign: "left",
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-end",
          backgroundColor: "red.500",
          borderBottomLeftRadius: 5,
          borderBottomRightRadius: 5,
          height: 70,
          padding: 3,
        }}
      >
        {/* {calculatorValue} */}
        as
      </Box>
      <Box
        sx={{
          paddingTop: 1,
          display: "grid",
          gridTemplateColumns: "repeat(4,1fr)",
          gridTemplateRows: "repeat(4,1fr)",
          gap: 1,
        }}
      >
        {btnValues.flat().map((btn: string | number, i: number) => (
          <Button
            onClick={(e) => handleClick((e.target as HTMLInputElement).value)}
            key={i}
            className="btnC"
            value={btn}
            // onClick={(e) => buttonClickHandler(e, btn)}
            sx={{
              height: "50px",
              borderRadius: 3,
              backgroundColor: `${btn !== "C" ? "red.400" : "blue.400"}`,
            }}
          >
            {btn}
          </Button>
        ))}
      </Box>
      <Button
        onClick={close}
        sx={{
          height: "50px",
          backgroundColor: "blue.500",
          width: "100%",
          marginTop: 3,
        }}
      >
        Simpan
      </Button>
    </Container>
  );
};

export default CalculatorFormula;
