import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalOverlay,
} from "@chakra-ui/react";
import React, { useState } from "react";
import CalculatorFormula from "../calculator/CalculatorFormula";

type Props = {};

const ModalCalculator = (props: Props) => {
  const [isOpen, onClose] = useState(false);
  return (
    <>
      <Button onClick={() => onClose(!isOpen)}>Open</Button>
      <Modal
        autoFocus={false}
        closeOnOverlayClick={false}
        isCentered
        isOpen={isOpen}
        onClose={() => {}}
      >
        <ModalOverlay />
        <ModalContent
          sx={{ backgroundColor: "transparent", boxShadow: "none" }}
        >
          <ModalBody>
            <CalculatorFormula close={async () => onClose(!isOpen)} />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default ModalCalculator;
