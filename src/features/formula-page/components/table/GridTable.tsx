import { GridItem } from "@chakra-ui/react";
import React from "react";

type Props = {
  children: JSX.Element;
  changeHeight?: boolean;
};

const GridTable = ({ children, changeHeight }: Props) => {
  return (
    <GridItem
      sx={{
        alignContent: "center",
        display: "grid",
        width: "100%",
        height: `${changeHeight ? "8" : "10"}`,
      }}
    >
      {children}
    </GridItem>
  );
};

export default GridTable;
