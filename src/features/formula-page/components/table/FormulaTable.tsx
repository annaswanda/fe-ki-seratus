import {
  Box,
  Button,
  Flex,
  Grid,
  NumberInput,
  NumberInputField,
  Table,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import ModalCalculator from "../modal/ModalCalculator";
import GridTable from "./GridTable";
type Props = {};

const FormulaTable = (props: Props) => {
  return (
    <div style={{ padding: 10 }}>
      <Flex justify="flex-end">
        <Button>Tambah</Button>
      </Flex>
      <TableContainer>
        <Table variant="simple">
          <Thead>
            <Tr>
              <Th>Nama Formula</Th>
              <Th>Formula</Th>
              <Th>hasil</Th>
            </Tr>
          </Thead>
          <Tbody>
            <Tr>
              <Td width="xs">formula name</Td>
              <Td width="xs">
                <Flex justify="space-between" align="items-center">
                  <Grid
                    templateColumns="repeat(6, 1fr)"
                    gap={3}
                    sx={{ width: "100px", textAlign: "center" }}
                  >
                    <GridTable>
                      <NumberInput size="xs" isDisabled>
                        <NumberInputField />
                      </NumberInput>
                    </GridTable>
                    <GridTable>
                      <b style={{ fontSize: 20 }}>*</b>
                    </GridTable>
                    <GridTable>
                      <NumberInput size="xs" isDisabled>
                        <NumberInputField />
                      </NumberInput>
                    </GridTable>
                    <GridTable changeHeight>
                      <b style={{ fontSize: 20 }}>+</b>
                    </GridTable>
                    <GridTable>
                      <NumberInput size="xs" isDisabled>
                        <NumberInputField />
                      </NumberInput>
                    </GridTable>
                  </Grid>
                  <ModalCalculator />
                </Flex>
              </Td>
              <Td>
                <Flex justify="space-between">
                  <Grid
                    templateColumns="repeat(8, 1fr)"
                    gap={3}
                    sx={{ width: "100px", textAlign: "center" }}
                  >
                    <GridTable>
                      <NumberInput size="xs">
                        <NumberInputField />
                      </NumberInput>
                    </GridTable>
                    <GridTable>
                      <b style={{ fontSize: 20 }}>*</b>
                    </GridTable>
                    <GridTable>
                      <NumberInput size="xs">
                        <NumberInputField />
                      </NumberInput>
                    </GridTable>
                    <GridTable changeHeight>
                      <b style={{ fontSize: 20 }}>+</b>
                    </GridTable>
                    <GridTable>
                      <NumberInput size="xs">
                        <NumberInputField />
                      </NumberInput>
                    </GridTable>
                    <GridTable>
                      <b style={{ fontSize: 20 }}> = </b>
                    </GridTable>
                    <GridTable>
                      <Box>0123</Box>
                    </GridTable>
                  </Grid>
                  <Button>Calculate</Button>
                </Flex>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default FormulaTable;
