import axios from "axios";
import { useState, useEffect } from "react";
import BarChart from "./bar-chart/BarChart";
import LineChart from "./line-chart/LineChart";
import PieChart from "./pie-chart/PieChart";

const Informasi = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const options = {
        method: "GET",
        url: "https://covid-19-statistics.p.rapidapi.com/provinces",
        params: { iso: "CHN" },
        headers: {
          "X-RapidAPI-Key":
            "65584d70edmshbb0a8d694f16f53p17c232jsnb6c5d723212c",
          "X-RapidAPI-Host": "covid-19-statistics.p.rapidapi.com",
        },
      };

      try {
        const response = await axios.request(options);
        setData(response.data.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="App">
      <div style={{ width: "100%", margin: "0 auto" }}>
        <div style={{marginTop: "50px"}}>
        <BarChart
          chartData={{
            labels: data.map((item) => item.province),
            datasets: [
              {
                label: "Data BarChart",
                data: data.map((item) => item.long),
                backgroundColor: [
                  "rgba(75,192,192,1)",
                  "#ecf0f1",
                  "#50AF95",
                  "#f3ba2f",
                  "#2a71d0",
                ],
                borderColor: "black",
                borderWidth: 2,
              },
            ],
          }}
        />

          
        </div>
        <div style={{ marginTop: "120px" }}>
          <LineChart
            chartData={{
              labels: data.map((item) => item.province),
              datasets: [
                {
                  label: "Data LineChart",
                  data: data.map((item) => item.lat),
                  backgroundColor: [
                    "rgba(75,192,192,1)",
                    "#ecf0f1",
                    "#50AF95",
                    "#f3ba2f",
                    "#2a71d0",
                  ],
                  borderColor: "black",
                  borderWidth: 2,
                },
              ],
            }}
          />
        </div>
        <div style={{marginTop:"120px"}}>
          <PieChart
            chartData={{
              labels: data.slice(0, 4).map((item) => item.lat),
              datasets: [
                {
                  label: "Data PieChart",
                  data: data.slice(0, 4).map((item) => item.long),
                  backgroundColor: [
                    "rgba(75,192,192,1)",
                    "#ecf0f1",
                    "#50AF95",
                    "#f3ba2f",
                    "#2a71d0",
                  ],
                  borderColor: "black",
                  borderWidth: 2,
                },
              ],
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default Informasi;
