import { useState } from "react";
import { Formula, IFormula } from "../../domain/model/formula";

export function useFormula() {
    const [formulas, setFormulas] = useState<IFormula[]>([]);
    const [savedFormulas, setSavedFormulas] = useState<IFormula[]>([]);
    const [showCalc, setShowCalc] = useState(false);
    const onType = (char: string | number) => {
        if (
            formulas[(formulas.length || 0) - 1]?.value === char ||
            (formulas[(formulas.length || 0) - 1]?.type === "operator" &&
                typeof char === "string")
        ) {
        } else if (
            formulas[(formulas.length || 0) - 1]?.type === "number" &&
            typeof char === "number"
        ) {
            setFormulas((formulas) =>
                formulas.map((item, i) =>
                    i === (formulas.length || 0) - 1
                        ? Formula.create(item)
                              .addNumber(Number(char))
                              .unmarshall()
                        : item
                )
            );
        } else {
            setFormulas((formulas) => [
                ...formulas,
                Formula.create({
                    type: typeof char == "string" ? "operator" : "number",
                    value: `${char}`,
                }).unmarshall(),
            ]);
        }
    };
    const onCompose = () => {
        setShowCalc(true);
    };
    const onSave = () => {
        setSavedFormulas(formulas);
        setShowCalc(false);
    };
    return {
        formulas,
        savedFormulas,
        showCalc,
        onCompose,
        onType,
        onSave,
    };
}
