import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    FormControl,
    FormLabel,
    Input,
    ModalFooter,
    Button,
    Select,
} from "@chakra-ui/react";
import React from "react";
import { useHalamanTabelModel } from "../HalamanTabelModel";

const ModalTabel = ({ isOpen, onOpen, onClose, tableModel }) => {
    const initialRef = React.useRef();
    const finalRef = React.useRef();
    return (
        <>
            <Modal
                initialFocusRef={initialRef}
                finalFocusRef={finalRef}
                isOpen={isOpen}
                onClose={onClose}
            >
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Create your table</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody pb={6}>
                        <FormControl>
                            <FormLabel>row</FormLabel>
                            <Input
                                name="row"
                                value={tableModel.table.row}
                                onChange={tableModel.handleTableGenerate}
                            />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>col</FormLabel>
                            <Input
                                placeholder="col"
                                name="col"
                                value={tableModel.table.col}
                                onChange={tableModel.handleTableGenerate}
                                ref={initialRef}
                            />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>position header</FormLabel>
                            <Select
                                name="role"
                                value={tableModel.table.role}
                                onChange={tableModel.handleTableGenerate}
                                ref={initialRef}
                            >
                                <option value="row">Row</option>
                                <option value="col">Col</option>
                            </Select>
                        </FormControl>
                    </ModalBody>

                    <ModalFooter>
                        <Button
                            colorScheme="blue"
                            mr={3}
                            onClick={tableModel.handleCreateTable}
                        >
                            Save
                        </Button>
                        <Button onClick={onClose}>Cancel</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    );
};

export default ModalTabel;
