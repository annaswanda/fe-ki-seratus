import {
    Box,
    Button,
    Flex,
    Spacer,
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    HStack,
    Container,
    useDisclosure,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import ModalTabel from "./modal/ModalTabel";
import { useHalamanTabelModel } from "./HalamanTabelModel";
const HalamanTabel = () => {
    const tableModal = useHalamanTabelModel();
    const { isOpen, onOpen, onClose } = useDisclosure();

    return (
        <>
            <Container maxW="8xl">
                <Box>
                    <Box marginBottom={"150px"}>
                        <HStack>
                            <Button onClick={onOpen} colorScheme="blue">
                                Config
                            </Button>

                            <Button colorScheme="blue">Save</Button>
                        </HStack>
                    </Box>

                    <Box>
                        <TableContainer>
                            <Table variant="simple">
                                <Tbody>
                                    {tableModal.data?.map((col, e) => {
                                        return (
                                            <Tr key={e}>
                                                {col.map((row, i) => {
                                                    if (
                                                        tableModal.table
                                                            .role === "col"
                                                    ) {
                                                        if (i === 0) {
                                                            return (
                                                                <Td>Input</Td>
                                                            );
                                                        } else {
                                                            return <Td>-</Td>;
                                                        }
                                                    } else {
                                                        if (e === 0) {
                                                            return (
                                                                <Td>Input</Td>
                                                            );
                                                        } else {
                                                            return <Td>-</Td>;
                                                        }
                                                    }
                                                })}
                                            </Tr>
                                        );
                                    })}
                                </Tbody>
                            </Table>
                        </TableContainer>
                    </Box>
                </Box>
            </Container>
            <ModalTabel
                isOpen={isOpen}
                onOpen={onOpen}
                onClose={onClose}
                tableModel={tableModal}
            />
        </>
    );
};

export default HalamanTabel;
