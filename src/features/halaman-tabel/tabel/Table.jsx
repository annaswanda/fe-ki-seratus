import React, { useState } from "react";

const TableMaker = ({ baris, kolom, posisi }) => {
    function generateTable({ numRows, numCols, headerPosition }) {
        // Inisialisasi variabel untuk menyimpan isi tabel dan header
        let tableContent = "";
        let tableHeader = "";

        // Loop untuk membuat header
        for (let i = 0; i < numCols; i++) {
            tableHeader += `<th>Header ${i + 1}</th>`;
        }

        // Loop untuk membuat isi tabel
        for (let i = 0; i < numRows; i++) {
            let rowContent = "";
            for (let j = 0; j < numCols; j++) {
                rowContent += `<td>Row ${i + 1}, Col ${j + 1}</td>`;
            }
            tableContent += `<tr>${rowContent}</tr>`;
        }

        // Menentukan posisi header
        if (headerPosition === "row") {
            tableContent = `<tr>${tableHeader}</tr>${tableContent}`;
        } else if (headerPosition === "col") {
            let newTableContent = "";
            for (let i = 0; i < numCols; i++) {
                let colContent = "";
                for (let j = 0; j < numRows; j++) {
                    colContent += `<tr><td>Row ${j + 1}, Col ${
                        i + 1
                    }</td></tr>`;
                }
                newTableContent += `<td><table>${tableHeader}${colContent}</table></td>`;
            }
            tableContent = `<tr>${newTableContent}</tr>`;
        }

        // Menghasilkan tabel JSX
        return (
            <table>
                {headerPosition === "none" ? null : (
                    <thead>{tableHeader}</thead>
                )}
                <tbody>{tableContent}</tbody>
            </table>
        );
    }

    return (
        <div>
            {generateTable({
                numRows: baris,
                numCols: kolom,
                headerPosition: posisi,
            })}
        </div>
    );
};

export default TableMaker;
