import { useState } from "react";
export const useHalamanTabelModel = () => {
    const [table, setTable] = useState({
        row: "",
        col: "",
        role: "",
    });
    const [data, setData] = useState<string[][]>();

    const handleTableGenerate = (e: any): void => {
        const name = e.target.name;
        const value = e.target.value;
        setTable({ ...table, [name]: value });
    };

    const createTable = (): void => {
        const result: string[][] = [];

        for (let i = 0; i < parseInt(table.col); i++) {
            const temp = [];
            for (let j = 0; j < parseInt(table.row); j++) {
                temp.push("row");
            }
            result.push(temp);
        }

        setData(result);
    };

    const handleCreateTable = (e: any): any => {
        e.preventDefault();
        if (!table.row) return false;
        if (!table.col) return false;
        setData([]);
        createTable();
        console.log(data, "data");
    };

    return { table, handleCreateTable, data, handleTableGenerate };
};
