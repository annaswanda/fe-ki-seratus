import { Box, Text, Button } from "@chakra-ui/react";
import { Breadcrumbs } from "../../app/common/components/Breadcrumbs";

export const Dashboard = () => {
  const name = localStorage.getItem("name");
  return (
    <Box display="flex" flexDirection="column" gap="30px">
      <Breadcrumbs items={["Dashboard"]} />
      <Text>Selamat Datang di Aplikasi KiSeratus, {name}!</Text>
    </Box>
  );
};
