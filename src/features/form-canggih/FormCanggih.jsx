import React from "react";
import {
    FormLabel,
    FormControl,
    Box,
    Stack,
    Input,
    NumberInput,
    NumberInputField,
    NumberIncrementStepper,
    NumberDecrementStepper,
    NumberInputStepper,
    Select,
    Textarea,
    Button,
} from "@chakra-ui/react";
const FormCanggih = () => {
    return (
        <>
            <Box width={"50%"} margin="auto">
                <form>
                    <FormControl marginBottom={3}>
                        <Stack>
                            <FormLabel>Nama</FormLabel>
                            <Input
                                variant="outline"
                                placeholder="Nama lengkap...    "
                            />
                        </Stack>
                    </FormControl>

                    <FormControl marginBottom={3}>
                        <Stack>
                            <FormLabel>Usia</FormLabel>
                            <NumberInput max={200} min={1}>
                                <NumberInputField id="amount" />
                                <NumberInputStepper>
                                    <NumberIncrementStepper />
                                    <NumberDecrementStepper />
                                </NumberInputStepper>
                            </NumberInput>
                        </Stack>
                    </FormControl>

                    <FormControl marginBottom={3}>
                        <Stack>
                            <FormLabel>Status perkawinan</FormLabel>
                            <Select>
                                <option value="option1">Belum Kawin</option>
                                <option value="option2">Kawin</option>
                            </Select>
                        </Stack>
                    </FormControl>

                    <FormControl marginBottom={3}>
                        <Stack>
                            <FormLabel>Alamat</FormLabel>
                            <Textarea
                                placeholder="Tuliskan alamat lengkap anda"
                                size="sm"
                            />
                        </Stack>
                    </FormControl>

                    <FormControl marginBottom={3}>
                        <Stack>
                            <FormLabel>Pekerjaan</FormLabel>
                            <Input
                                variant="outline"
                                placeholder="Web Developer"
                            />
                        </Stack>
                    </FormControl>

                    <FormControl marginBottom={6}>
                        <Stack>
                            <FormLabel>Ringkasan Kehidupan</FormLabel>
                            <Textarea
                                placeholder="Ceritakan secara ringkas dan jelas ringkasan kehidupan anda"
                                size="sm"
                            />
                        </Stack>
                    </FormControl>

                    <FormControl>
                        <FormLabel htmlFor="upload-file" width={10}>
                            <Box
                                width={300}
                                height={300}
                                border={2}
                                borderInline={"dashed"}
                                borderColor={"#E2E8F0"}
                                borderTop={"dashed"}
                                borderBottom={"dashed"}
                                borderTopColor={"#E2E8F0"}
                                borderBottomColor={"#E2E8F0"}
                                display={"flex"}
                                justifyContent={"center"}
                                borderRadius={15}
                            >
                                <Box margin={"auto"}>Upload your image</Box>
                            </Box>
                        </FormLabel>
                        <Input
                            type={"file"}
                            id="upload-file"
                            display={"none"}
                            width={1}
                        />
                    </FormControl>

                    <Button marginTop={10} colorScheme="blue" size="lg">
                        Tambahkan
                    </Button>
                </form>
            </Box>
        </>
    );
};

export default FormCanggih;
