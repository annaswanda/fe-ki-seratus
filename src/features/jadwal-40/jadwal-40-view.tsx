import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
import interactionPlugin from "@fullcalendar/interaction" // needed for dayClick
import useJadwal40 from "./jadwal-40-view-model";

export const Jadwal40View = () => {
    const jadwal40 = useJadwal40();
    return (
        <FullCalendar
            plugins={[dayGridPlugin, interactionPlugin ]}
            initialView="dayGridMonth"
            dateClick={jadwal40.onShowDate}
        />
    );
};
