import { useFormik } from "formik";
import { useState } from "react";
import { IUser } from "../../domain/model/user";
import { useToast } from "@chakra-ui/react";
import { useNavigate } from "react-router";

export interface IUseLogin {
  user: IUser;
  error?: string | null;
  onUserChange: unknown;
  submit: unknown;
}

const akun = {
  id_login: "bima",
  password: "password",
  role: "Admin",
  name: "Bima Ap",
};

export function useLogin() {
  const navigate = useNavigate();
  const toast = useToast();
  const [error, setError] = useState<string | null>();
  const formik = useFormik<IUser>({
    initialValues: {
      id_login: "",
      name: "",
      password: "",
      role: null,
    },
    onSubmit: (values) => {
      if (values?.id_login !== akun?.id_login) {
        toast({
          position: "top",
          title: "Akun belum terdaftar!",
          status: "error",
          duration: 5000,
          isClosable: true,
        });
        return;
      }

      if (values?.password !== akun?.password) {
        toast({
          position: "top",
          title: "Password salah!",
          status: "error",
          duration: 5000,
          isClosable: true,
        });
        return;
      }

      localStorage.setItem("token", "token-123")
      localStorage.setItem("role", akun?.role)
      localStorage.setItem("name", akun?.name)
      navigate("/");
    },
  });
  const onUserChange = formik.handleChange;
  return {
    user: formik.values,
    error,
    onUserChange,
    submit: formik.submitForm,
  };
}
