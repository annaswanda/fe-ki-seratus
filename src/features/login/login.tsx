import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Heading,
  Image,
  Input,
  VStack,
} from "@chakra-ui/react";
import Logo from "../../app/common/image/Logo.jpg";
import { useLogin } from "./login-view-model";

export const Login = () => {
  const login = useLogin();
  
  return (
    <>
      <Box bg="#00A3C4" h="100vh" display="flex" alignItems="center">
        <Box
          w=" 447px"
          h=" 615px"
          p={[8, 10]}
          mx="auto"
          borderWidth={1}
          borderColor="gray.300"
          borderRadius={8}
          bg="white"
        >
          <VStack
            spacing={4}
            align={["flex-start", " center"]}
            w="full"
            paddingTop="5"
            paddingBottom="20"
          >
            <Image src={Logo} alt="Dan Abramov" />
            <Heading paddingTop="5">Login</Heading>
            <FormControl>
              <FormLabel htmlFor="email" fontWeight="bold">
                ID
              </FormLabel>
              <Input
                id="email"
                type="text"
                placeholder="Masukan ID"
                name="id_login"
                value={login.user.id_login}
                onChange={login.onUserChange}
              />
            </FormControl>
            <FormControl>
              <FormLabel htmlFor="password" fontWeight="bold">
                Password
              </FormLabel>
              <Input
                id="password"
                type="password"
                placeholder="Masukan Password"
                name="password"
                value={login.user.password}
                onChange={login.onUserChange}
              />
            </FormControl>
            <Button bg="#00A3C4" color="white" w="full" onClick={login.submit} >
              Submit
            </Button>
          </VStack>
        </Box>
      </Box>
    </>
  );
};
