import { Outlet, useNavigate } from "react-router-dom";
import { Box, Text, Button } from "@chakra-ui/react";
import { Icon, HamburgerIcon } from "@chakra-ui/icons";
import {
  HiLogout,
  HiOutlineViewGrid,
  HiOutlineCalendar,
  HiOutlineDocumentText,
  HiOutlineTable,
  HiOutlineUserGroup,
  HiOutlineChartPie,
} from "react-icons/hi";
import { NavItem } from "../../app/common/components/NavItem";

export const AdminLayoutView = () => {
  const navigate = useNavigate();

  const navItems = [
    {
      icon: <Icon as={HiOutlineViewGrid} w="7" h="7" color="black" />,
      title: "Dashboard",
      link: "/",
    },
    {
      icon: <Icon as={HiOutlineCalendar} w="7" h="7" color="black" />,
      title: "Jadwal 4.0",
      link: "/jadwal-40",
    },
    {
      icon: <Icon as={HiOutlineUserGroup} w="7" h="7" color="black" />,
      title: "Silsilah Keluarga",
      link: "/silsilah-keluarga",
    },
    {
      icon: <Icon as={HiOutlineDocumentText} w="7" h="7" color="black" />,
      title: "Form Unik",
      link: "/form-unik",
    },
    {
      icon: <Icon as={HiOutlineDocumentText} w="7" h="7" color="black" />,
      title: "Form Formula",
      link: "/formula",
    },
    {
      icon: <Icon as={HiOutlineDocumentText} w="7" h="7" color="black" />,
      title: "Form Canggih",
      link: "/form-canggih",
    },
    {
      icon: <Icon as={HiOutlineTable} w="7" h="7" color="black" />,
      title: "Table 4.0",
      link: "/halaman-tabel",
    },
    {
      icon: <Icon as={HiOutlineChartPie} w="7" h="7" color="black" />,
      title: "Informasi",
      link: "/informasi",
    },
  ];
  return (
    <Box maxW="100vw" display="flex" overflowY="auto" bg="white">
      <Box w="100vw" pt="70px" pl="300px" display="flex">
        <Box p="30px" flex="1">
          <Outlet />
        </Box>
      </Box>
      <Box position="fixed" w="100vw" h="70px" pl="300px">
        <Box
          w="100%"
          h="100%"
          px="30px"
          bg="cyan.600"
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          boxShadow="xl"
        >
          <Button
            colorScheme="teal"
            variant="link"
            p="0px"
            m="0px"
            onClick={() => {
              alert("Hamburger Event");
            }}
          >
            <HamburgerIcon w="6" h="6" color="white" />
          </Button>
          <Button
            colorScheme="teal"
            variant="link"
            display="flex"
            alignItems="center"
            gap="5px"
            onClick={() => {
              localStorage.clear();
              navigate("login");
            }}
          >
            <Icon as={HiLogout} w="5" h="5" color="white" />
            <Text color="white" as="b">
              Logout
            </Text>
          </Button>
        </Box>
      </Box>
      <Box
        h="100vh"
        w="300px"
        bg="white"
        boxShadow="xl"
        position="fixed"
        top="0"
      >
        <Box
          h="70px"
          w="100%"
          borderBottom="2px"
          borderBottomColor="gray.200"
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Text fontSize="2xl" color="cyan.600" as="b">
            KISERATUS
          </Text>
        </Box>
        <Box p="30px" display="flex" flexDirection="column" gap="20px">
          {navItems?.map((item, i) => {
            return (
              <NavItem
                key={i}
                title={item?.title}
                icon={item?.icon}
                link={item?.link}
              />
            );
          })}
        </Box>
      </Box>
    </Box>
  );
};
