import { createBrowserRouter, Outlet } from "react-router-dom";
import { AdminLayoutView } from "../features/admin-layout/admin-layout-view";
import { Dashboard } from "../features/dashboard/dashboard";
import FormCanggih from "../features/form-canggih/FormCanggih";
import CreateFormPage from "../features/form-unik/create-form";
import PreviewForm from "../features/form-unik/preview-form";
import { FormulaView } from "../features/formula/formula-view";
import HalamanTabel from "../features/halaman-tabel/HalamanTabel";
import Informasi from "../features/informasi/Informasi";
import { Jadwal40View } from "../features/jadwal-40/jadwal-40-view";
import { Login } from "../features/login/login";

import { SilsilahKeluargaView } from "../features/silsilah-keluarga/silsilah-keluarga-view";

const Root = () => {
    return <Outlet />;
};

export const router = createBrowserRouter([
    {
        path: "/",
        element: <AdminLayoutView />,
        children: [
            {
                path: "",
                element: <Dashboard />,
            },
            {
                path: "/form-canggih",
                element: <FormCanggih />,
            },
            {
                path: "/halaman-tabel",
                element: <HalamanTabel />,
            },
            {
                path: "/informasi",
                element: <Informasi />,
            },
            {
                path: "/silsilah-keluarga",
                element: <SilsilahKeluargaView />,
            },
            {
                path: "formula",
                element: <FormulaView />,
            },
            {
                path: "form-unik",
                element: <CreateFormPage />,
            },
            {
                path: "add-forms/:id",
                element: <PreviewForm />,
            },
            {
                path: "jadwal-40",
                element: <Jadwal40View />,
            },
        ],
    },
    {
        path: "/login",
        element: <Login />,
    },
]);
