import { Button, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router";

export type INavItemProps = {
  icon: unknown;
  title: string;
  key: number;
  link: string;
};

export const NavItem = ({
  icon = null,
  title = "",
  key = 0,
  link = ""
}: INavItemProps) => {
  const navigate = useNavigate();
  return (
    <Button
      colorScheme="teal"
      variant="ghost"
      display="flex"
      alignItems="center"
      justifyContent="start"
      gap="8px"
      key={key}
      cursor="pointer"
      onClick={() => navigate(link)}
    >
      <>{icon}</>
      <Text as="b" fontSize="18px">
        {title}
      </Text>
    </Button>
  );
};
