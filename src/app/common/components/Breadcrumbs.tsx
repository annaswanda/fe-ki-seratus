import { Box, Text, Button } from "@chakra-ui/react";
import { Icon } from "@chakra-ui/icons";
import { HiOutlineHome } from "react-icons/hi";

export type IBreadcrumbsProps = {
  items: Array<any>;
};

export const Breadcrumbs = ({ items = [] }: IBreadcrumbsProps) => {
  return (
    <Box display="flex" alignItems="center" gap="10px">
      <Icon as={HiOutlineHome} w="6" h="6" color="black" />
      <Text>/</Text>
      {items?.map((item, i, arr) => {
        return (
          <Box display="flex" alignItems="center" gap="15px">
            <Text key={i}>{item}</Text>
            {arr?.length - 1 != i ? <Text>/</Text> : null}
          </Box>
        );
      })}
    </Box>
  );
};
